FROM alpine:latest

RUN apk update && \
    apk add --no-cache openjdk17-jdk && \
    rm -rf /var/cache/apk/*
	
COPY /spring-backend /
WORKDIR /

RUN ./mvnw -f /pom.xml clean package 

RUN adduser -s -H -D user 

USER user

ENTRYPOINT ["java", "-jar", "/target/hobbie-backend-0.0.1-SNAPSHOT.jar"]